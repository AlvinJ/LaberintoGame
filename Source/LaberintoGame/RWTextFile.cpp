// Fill out your copyright notice in the Description page of Project Settings.

#include "RWTextFile.h"
#include "LaberintoGame.h"
#include "FileHelper.h"
#include "Paths.h"

bool URWTextFile::SaveTxt(FString SaveTextB, FString FileNameB)
{
	return FFileHelper::SaveStringToFile(SaveTextB, *(FPaths::GameDir() + FileNameB));
}

bool URWTextFile::LoadTxt(FString FileNameA, FString& TextContent)
{
	return FFileHelper::LoadFileToString(TextContent, *(FPaths::GameDir() + FileNameA));
}



